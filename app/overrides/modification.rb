Deface::Override.new(:virtual_path => 'spree/admin/products/new',
 :insert_after => "div[data-hook='new_product_shipping_category']",
 :partial       => "/product_taxon",
 :name          => "product_taxons")